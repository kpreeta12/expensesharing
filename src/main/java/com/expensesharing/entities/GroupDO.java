package com.expensesharing.entities;

import javax.persistence.*;
import java.util.Objects;

/**
 * Created by preeta kuruvilla on 26/08/2018
 */
@Entity
public class GroupDO {

    @Id
    private Long groupId;
    private String groupName;
    private String eventDescription;
    private int numberOfParticipants;
    private double totalExpense;
    private double totalSpent;


    protected GroupDO() {}

    public GroupDO(long groupId, String groupName, String eventDescription, int numberOfParticipants, double totalExpense, double totalSpent) {
        this.groupId=groupId;
        this.groupName=groupName;
        this.eventDescription=eventDescription;
        this.numberOfParticipants=numberOfParticipants;
        this.totalExpense=totalExpense;
        this.totalSpent=totalSpent;
    }

    
    public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getEventDescription() {
		return eventDescription;
	}

	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}

	public int getNumberOfParticipants() {
		return numberOfParticipants;
	}

	public void setNumberOfParticipants(int numberOfParticipants) {
		this.numberOfParticipants = numberOfParticipants;
	}

	public double getTotalExpense() {
		return totalExpense;
	}

	public void setTotalExpense(double totalExpense) {
		this.totalExpense = totalExpense;
	}

	public double getTotalSpent() {
		return totalSpent;
	}

	public void setTotalSpent(double totalSpent) {
		this.totalSpent = totalSpent;
	}

	@Override
    public String toString() {
        return String.format("GroupDO[group_id=[%s], group_name=[%s], event_description=[%s], number_of_participants=[%s], total_expense=[%s]]",
                this.groupId,
                this.groupName,
                this.eventDescription,
                this.numberOfParticipants,
                this.totalExpense
                );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupDO group = (GroupDO) o;
        return Objects.equals(groupId, group.groupId) &&
                Objects.equals(groupName, group.groupName) &&
                Objects.equals(eventDescription, group.eventDescription) &&
                Objects.equals(numberOfParticipants, group.numberOfParticipants) &&
                Objects.equals(totalExpense, group.totalExpense);
    }

    @Override
    public int hashCode() {
        int result = 0;
        if (this == null)
            return result;
        result = 31 * result + (this.groupId == null ? 0 : this.groupId.hashCode());
        result = 31 * result + (this.groupName == null ? 0 : this.groupName.hashCode());
        result = 31 * result + (this.eventDescription == null ? 0 : this.eventDescription.hashCode());
        return result;
    }

	
}
