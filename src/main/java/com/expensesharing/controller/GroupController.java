package com.expensesharing.controller;

import com.expensesharing.services.GroupService;
import com.expensesharing.views.GroupVO;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by preeta kuruvilla on 8/26/2018.
 */

@RestController
@RequestMapping("/Groups")
public class GroupController {

    private final GroupService groupService;

    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<GroupVO> listGroups() {
        return groupService.listGroups();
    }

    @RequestMapping(path = "/{groupId}", produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.GET)
    public GroupVO getGroup(@PathVariable final Long groupId) {
        return groupService.getGroup(groupId);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody GroupVO createGroup(@RequestBody final GroupVO groupVO) {
        return groupService.createGroup(groupVO);
    }

}
