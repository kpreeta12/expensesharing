package com.expensesharing.controller;

import com.expensesharing.services.ParticipantService;
import com.expensesharing.views.GiverSettlementVO;
import com.expensesharing.views.ParticipantSettlementVO;
import com.expensesharing.views.ParticipantSpendVO;
import com.expensesharing.views.ParticipantVO;
import com.expensesharing.views.ReceiverSettlementVO;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by preeta kuruvilla on 27/08/2018.
 */
@RestController
@RequestMapping("/Participants")
public class ParticipantController {

    private ParticipantService participantService;

    public ParticipantController(ParticipantService participantService) {
        this.participantService = participantService;
    }

    @RequestMapping(path = "/{participantId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ParticipantVO getParticipant(@PathVariable final Long participantId) {
        return participantService.getParticipant(participantId);
    }
    
    @RequestMapping(path = "all/{groupId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ParticipantVO> listParticipants(@PathVariable final Long groupId) {
        return participantService.listParticipantsByGroupId(groupId);
    }

    @RequestMapping(path = "/{participantId}", method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void deleteParticipant(@PathVariable final Long participantId) {
        participantService.deleteParticipant(participantId);
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ParticipantVO createParticipant(@RequestBody final ParticipantVO participantVO) {
        return participantService.createParticipant(participantVO);
    }
    
    @RequestMapping(path = "/spend", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public  void updateLiabilityAmtBasedOnSpent(@RequestBody final ParticipantSpendVO participantSpendVO) {
       participantService.updateLiabilityAmtBasedOnSpent(participantSpendVO);
    }
    
    @RequestMapping(path = "/settlement", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public  void updateLiabilityAmtBasedOnSettlement(@RequestBody final ParticipantSettlementVO participantSettlementVO) {
       participantService.updateLiabilityAmtBasedOnSettlement(participantSettlementVO);
    }
    

}
