package com.expensesharing.repositories;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import com.expensesharing.entities.ParticipantDO;

/**
 * Created by preeta kuruvilla on 26/08/2018.
 */
public interface ParticipantRepository extends CrudRepository<ParticipantDO, Long> {
    List<ParticipantDO> findByGroupId(Long group_id);
}
