package com.expensesharing.repositories;

import com.expensesharing.entities.GroupDO;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by preeta kuruvilla on 26/08/2018.
 */
public interface GroupRepository extends CrudRepository<GroupDO, Long> {
}
