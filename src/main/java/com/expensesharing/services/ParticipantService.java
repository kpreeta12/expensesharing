package com.expensesharing.services;

import com.expensesharing.views.GiverSettlementVO;
import com.expensesharing.views.ParticipantSettlementVO;
import com.expensesharing.views.ReceiverSettlementVO;
import com.expensesharing.views.ParticipantSpendVO;
import com.expensesharing.views.ParticipantVO;

import java.util.List;

/**
 * Created by preeta kuruvilla on 26/08/2018.
 */
public interface ParticipantService {

    ParticipantVO createParticipant(final ParticipantVO ParticipantVO);
    
    void updateLiabilityAmtBasedOnSpent(ParticipantSpendVO participantVO);
    
    void updateLiabilityAmtBasedOnSettlement(ParticipantSettlementVO participantSettlementVO);
    
    void updateGiverLiabilityAmtBasedOnSettlement(GiverSettlementVO giverSettlementVO);

    void updateReceiverLiabilityAmtBasedOnSettlement(ReceiverSettlementVO receiverSettlementVO);
    
    ParticipantVO getParticipant(final Long ParticipantId);

    void deleteParticipant(final Long ParticipantId);

    List<ParticipantVO> listParticipantsByGroupId(final Long groupId);
}
