package com.expensesharing.services;

import com.expensesharing.views.GroupVO;
import com.expensesharing.views.ParticipantVO;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.*;

import java.util.List;

/**
 * Created by preeta kuruvilla on 26/08/2018.
 */

public interface GroupService {

    GroupVO getGroup(Long GroupId);

    List<GroupVO> listGroups();
    
    GroupVO createGroup(final GroupVO GroupVO);

}
