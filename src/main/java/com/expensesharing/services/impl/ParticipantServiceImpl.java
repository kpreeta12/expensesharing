package com.expensesharing.services.impl;

import com.expensesharing.entities.GroupDO;
import com.expensesharing.entities.ParticipantDO;
import com.expensesharing.exception.GroupNotFoundException;
import com.expensesharing.exception.ParticipantNotFoundException;
import com.expensesharing.repositories.GroupRepository;
import com.expensesharing.repositories.ParticipantRepository;
import com.expensesharing.services.ParticipantService;
import com.expensesharing.utils.ValidationUtils;
import com.expensesharing.utils.mapping.GroupMappingFunction;
import com.expensesharing.utils.mapping.ParticipantMappingFunction;
import com.expensesharing.views.ReceiverSettlementVO;
import com.expensesharing.views.GiverSettlementVO;
import com.expensesharing.views.ParticipantSettlementVO;
import com.expensesharing.views.ParticipantSpendVO;
import com.expensesharing.views.ParticipantVO;
import com.google.common.collect.ImmutableList;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by preeta kuruvilla on 26/08/2018.
 */
@Service
@Transactional
public class ParticipantServiceImpl implements ParticipantService {

    private final ParticipantRepository participantRepository;
    private final GroupRepository groupRepository;
    private static final Logger logger = LoggerFactory.getLogger(GroupServiceImpl.class);
    
    public ParticipantServiceImpl(ParticipantRepository participantRepository, GroupRepository groupRepository) {
        this.participantRepository = participantRepository;
        this.groupRepository = groupRepository;
    }

    @Override
    public ParticipantVO createParticipant(ParticipantVO participantVO) {

        ValidationUtils.validateParam(participantVO.getParticipantId(),
                String.format("Error input value orderId=[%s]", participantVO.getParticipantId()));
        GroupDO groupDO = groupRepository.findOne(participantVO.getGroupId());
        if (groupDO == null) {
            throw new GroupNotFoundException(String.format("GroupDO with id=[%s], not found",
                    participantVO.getGroupId()));
        }
        if(checkNumberOfParticipants(participantVO.getGroupId()).equals("LESSER")) {
        ParticipantDO participantDO = new ParticipantDO(participantVO.getParticipantId(),participantVO.getParticipantName(),participantVO.getGroupId(),participantVO.getLiabilityAmount());       
        participantDO.setLiabilityAmount(groupDO.getTotalExpense()/groupDO.getNumberOfParticipants());
        participantRepository.save(participantDO);
        return new ParticipantMappingFunction().apply(participantDO);
        }
        else {
        	throw new GroupNotFoundException(String.format("GroupDO with id=[%s], already reached max number of participants",
                    participantVO.getGroupId()));
        }
    }
    
    @Override
    public void updateLiabilityAmtBasedOnSpent(ParticipantSpendVO participantSpendVO) {

        ValidationUtils.validateParam(participantSpendVO.getParticipantId(),
                String.format("Error input value participantId=[%s]", participantSpendVO.getParticipantId()));
        ParticipantDO participantDO = participantRepository.findOne(participantSpendVO.getParticipantId());
        if (participantDO == null) {
            throw new ParticipantNotFoundException(String.format("ParticipantDO with id=[%s], not found",
                    participantSpendVO.getParticipantId()));
        }
        GroupDO groupDO=groupRepository.findOne(participantDO.getGroupId());        
        if(checkNumberOfParticipants(participantDO.getGroupId()).equals("EQUAL")){  
        participantDO.setLiabilityAmount(participantDO.getLiabilityAmount()-participantSpendVO.getSpendAmount());       
        groupDO.setTotalSpent(groupDO.getTotalSpent()+participantSpendVO.getSpendAmount());
        if(checkTotalExpenseSpent(groupDO.getGroupId()).equals("LESSER") || checkTotalExpenseSpent(groupDO.getGroupId()).equals("EQUAL")) {
        participantRepository.save(participantDO);
        new ParticipantMappingFunction().apply(participantDO);
        groupRepository.save(groupDO);
        new GroupMappingFunction().apply(groupDO);
        }
        else {
        	throw new GroupNotFoundException(String.format("GroupDO with id=[%s], the spend is more than expense",
                    groupDO.getGroupId()));
        }
        }
        else {
        	throw new ParticipantNotFoundException(String.format("ParticipantDO with id=[%s], number of participants not equal to max participants for the group",
                    participantSpendVO.getParticipantId()));
        }
    }
    
    @Override
    public void updateGiverLiabilityAmtBasedOnSettlement(GiverSettlementVO giverSettlementVO) {

    	ValidationUtils.validateParam(giverSettlementVO.getGiverParticipantId(),
        String.format("Error input value participantId=[%s]", giverSettlementVO.getGiverParticipantId()));
        ParticipantDO participantDO1 = participantRepository.findOne(giverSettlementVO.getGiverParticipantId());
        if (participantDO1 == null) {
            throw new ParticipantNotFoundException(String.format("ParticipantDO with id=[%s], not found",
                    giverSettlementVO.getGiverParticipantId()));
        }
        GroupDO groupDO=groupRepository.findOne(participantDO1.getGroupId());
        if(checkTotalExpenseSpent(groupDO.getGroupId()).equals("EQUAL")) {
        participantDO1.setLiabilityAmount(participantDO1.getLiabilityAmount()-giverSettlementVO.getSettlementAmount());
        participantRepository.save(participantDO1);        
        new ParticipantMappingFunction().apply(participantDO1);
        }
        else {
        	throw new GroupNotFoundException(String.format("GroupDO with id=[%s], the settlement cannot be done before spending the total expense incurred",
                    groupDO.getGroupId()));
        }
    }
    
    @Override
    public void updateReceiverLiabilityAmtBasedOnSettlement(ReceiverSettlementVO receiverSettlementVO) {
    	
       ValidationUtils.validateParam(receiverSettlementVO.getReceiverParticipantId(),
        String.format("Error input value participantId=[%s]", receiverSettlementVO.getReceiverParticipantId()));
        ParticipantDO participantDO2 = participantRepository.findOne(receiverSettlementVO.getReceiverParticipantId());
        if (participantDO2 == null) {
            throw new ParticipantNotFoundException(String.format("ParticipantDO with id=[%s], not found",
                    receiverSettlementVO.getReceiverParticipantId()));
        }
        GroupDO groupDO=groupRepository.findOne(participantDO2.getGroupId());
        if(checkTotalExpenseSpent(groupDO.getGroupId()).equals("EQUAL")) {        
        participantDO2.setLiabilityAmount(participantDO2.getLiabilityAmount()+receiverSettlementVO.getSettlementAmount());
        participantRepository.save(participantDO2);               
        new ParticipantMappingFunction().apply(participantDO2);
        }
        else {
        	throw new GroupNotFoundException(String.format("GroupDO with id=[%s], the settlement cannot be done before spending the total expense incurred",
                    groupDO.getGroupId()));
        }
    }

    @Override
    public ParticipantVO getParticipant(Long participantId) {
        ValidationUtils.validateParam(participantId, String.format("Error input value ParticipantId=[%s]", participantId));
        final ParticipantDO participantDO = participantRepository.findOne(participantId);
        if (participantDO == null) {
            throw new ParticipantNotFoundException(String.format("ParticipantDO with id=[%s], not found", participantId));
        }
        return new ParticipantMappingFunction().apply(participantDO);
    }

    @Override
    public void deleteParticipant(Long participantId) {
        ValidationUtils.validateParam(participantId, String.format("Error input value ParticipantId=[%s]", participantId));
        participantRepository.delete(participantId);
    }

    @Override
    public List<ParticipantVO> listParticipantsByGroupId(Long groupId) {
        ValidationUtils.validateParam(groupId, String.format("Error input value groupId=[%s]", groupId));
        final List<ParticipantDO> participantListDO = ImmutableList.copyOf(participantRepository.findByGroupId(groupId));
        logger.info("List all Participant in a group=[{}]", participantListDO);
        return participantListDO.stream().map(new ParticipantMappingFunction()).collect(Collectors.<ParticipantVO>toList());
    }
    
    //utility functions
    
    public String checkNumberOfParticipants(Long groupId) {	
    	ValidationUtils.validateParam(groupId, String.format("Error input value groupId=[%s]", groupId));
        final List<ParticipantDO> participantListDO = ImmutableList.copyOf(participantRepository.findByGroupId(groupId));
        int participantCount=participantListDO.size();
        GroupDO groupDO=groupRepository.findOne(groupId);
        int numberOfParticipants=groupDO.getNumberOfParticipants();
        if(participantCount < numberOfParticipants)
        	return "LESSER";
        else if (participantCount > numberOfParticipants)
        	return "MORE";
        else
        	return "EQUAL";
    }
    
    
    public String checkTotalExpenseSpent(Long groupId) {
    	
    	ValidationUtils.validateParam(groupId, String.format("Error input value groupId=[%s]", groupId));
        GroupDO groupDO=groupRepository.findOne(groupId);
        double maxexpense=groupDO.getTotalExpense();
        double spend=groupDO.getTotalSpent();
        if(spend < maxexpense)
        	return "LESSER";
        else if (spend > maxexpense)
        	return "MORE";
        else
        	return "EQUAL";
    	
    }

	@Override
	@Transactional
	public void updateLiabilityAmtBasedOnSettlement(ParticipantSettlementVO participantSettlementVO) {
		GiverSettlementVO giverSettlementVO=new GiverSettlementVO(participantSettlementVO.getgiverparticipantId(),participantSettlementVO.getSettlementAmount());
		ReceiverSettlementVO receiverSettlementVO=new ReceiverSettlementVO(participantSettlementVO.getreceiverparticipantId(),participantSettlementVO.getSettlementAmount());
		updateGiverLiabilityAmtBasedOnSettlement(giverSettlementVO);
		updateReceiverLiabilityAmtBasedOnSettlement(receiverSettlementVO);
	
    }     
}
