package com.expensesharing.services.impl;

import com.google.common.collect.ImmutableList;
import com.expensesharing.entities.GroupDO;
import com.expensesharing.exception.GroupNotFoundException;
import com.expensesharing.repositories.GroupRepository;
import com.expensesharing.services.GroupService;
import com.expensesharing.utils.ValidationUtils;
import com.expensesharing.utils.mapping.GroupMappingFunction;
import com.expensesharing.views.GroupVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Created by preeta kuruvilla on 26/08/2018.
 */
@Service
@Transactional
public class GroupServiceImpl implements GroupService {

    private static final Logger logger = LoggerFactory.getLogger(GroupServiceImpl.class);

    GroupRepository groupRepository;

    public GroupServiceImpl(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

   @Override
    public GroupVO getGroup(Long groupId) {

        ValidationUtils.validateParam(groupId, String.format("Error input value GroupId=[%s]", groupId));
        final GroupDO GroupDO = groupRepository.findOne(groupId);

        if (GroupDO == null) {
            throw new GroupNotFoundException(String.format("GroupDO with id=[%s], not found", groupId));
        }

        return new GroupMappingFunction().apply(GroupDO);
    }

    @Override
    public List<GroupVO> listGroups() {
        final List<GroupDO> groupListDO = ImmutableList.copyOf(groupRepository.findAll());
        logger.info("List all Groups=[{}]", groupListDO);
        return groupListDO.stream().map(new GroupMappingFunction()).collect(Collectors.<GroupVO>toList());
    }

    @Override
    public GroupVO createGroup(GroupVO groupVO) {
        GroupDO groupDO = new GroupDO(groupVO.getGroupId(),groupVO.getGroupName(),groupVO.getEventDescription(),groupVO.getNumberOfParticipants(),groupVO.getTotalExpense(),groupVO.getTotalSpent());
        groupRepository.save(groupDO);
        return new GroupMappingFunction().apply(groupDO);
    }
}
