package com.expensesharing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	/*@Bean
	public CommandLineRunner demo(GroupRepository GroupRepository,
								  ParticipantRepository ParticipantRepository) {
		return (args) -> {
			GroupDO GroupOne = new GroupDO("john111", "John", "Smith");
			GroupDO GroupTwo = new GroupDO("billT1990", "Bill", "Turner");
			GroupDO GroupThree = new GroupDO("robin.alex", "Alex", "Robinson");



			Set<ParticipantDO> ParticipantsDO = Sets.newHashSet(new ParticipantDO("Table", orderOne, 1),
					new ParticipantDO("Chair", orderTwo, 4),
					new ParticipantDO("Door", orderThree, 2),
					new ParticipantDO("Desk", orderThree, 1),
					new ParticipantDO("Cabinet", orderFour, 2),
					new ParticipantDO("Bookshelf", orderFive, 1),
					new ParticipantDO("Bed", orderSix, 1),
					new ParticipantDO("Sofa", orderSeven, 2));

			GroupRepository.save(Lists.newArrayList(GroupOne, GroupTwo, GroupThree));
			orderRepository.save(Lists.newArrayList(
					orderOne, orderTwo, orderThree, orderFour, orderFive, orderSix, orderSeven));
			ParticipantRepository.save(ParticipantsDO);

			log.info("Show all Groups, with orders and Participants: ");
			log.info("-------------------");
			for (GroupDO GroupDO : GroupRepository.findAll()) {
				log.info(GroupDO.toString());
			}
		};


	}*/



}