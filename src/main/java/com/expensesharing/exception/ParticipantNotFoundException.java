package com.expensesharing.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by preeta kuruvilla on 26/08/2018.
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ParticipantNotFoundException extends RuntimeException {

    public ParticipantNotFoundException(String message) {
        super(message);
    }
}
