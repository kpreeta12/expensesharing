package com.expensesharing.views;

import javax.persistence.*;
import java.util.Objects;

/**
 * Created by preeta kuruvilla on 26/08/2018.
 */
@Entity
public class ParticipantSettlementVO {

    @Id
    private Long giverparticipantId;
    private Long receiverparticipantId;
    private double settlementAmount;

    protected ParticipantSettlementVO() {}

    public ParticipantSettlementVO(long giverparticipantId, long receiverparticipantI, double settlementAmount) {
      this.giverparticipantId=giverparticipantId;
      this.receiverparticipantId=receiverparticipantId;
      this.settlementAmount=settlementAmount;
    }


    public Long getgiverparticipantId() {
		return giverparticipantId;
	}

	 public void setgiverparticipantId(Long giverparticipantId) {
		this.giverparticipantId = giverparticipantId;
	}

  public Long getreceiverparticipantId() {
  return receiverparticipantId;
}

 public void setreceiverparticipantId(Long receiverparticipantId) {
  this.receiverparticipantId = receiverparticipantId;
}


	public double getSettlementAmount() {
		return settlementAmount;
	}

	public void setSettlementAmount(double settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	@Override
    public String toString() {
        return String.format("ParticipantSettlementVO[giverparticipantId=[%s],receiverparticipantId=[%s],settlementAmount=[%s]",
        this.giverparticipantId,
        this.receiverparticipantId,
        this.settlementAmount);
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParticipantSettlementVO participant = (ParticipantSettlementVO) o;
        return Objects.equals(giverparticipantId, participant.giverparticipantId) &&
               Objects.equals(receiverparticipantId, participant.receiverparticipantId) &&
                Objects.equals(settlementAmount, participant.settlementAmount);
    }

    @Override
    public int hashCode() {
        int result = 0;
        if (this == null)
            return result;
        result = 31 * result + (this.giverparticipantId == null ? 0 : this.giverparticipantId.hashCode());
        return result;
    }
}
