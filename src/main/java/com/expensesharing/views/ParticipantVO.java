package com.expensesharing.views;

import javax.persistence.*;
import java.util.Objects;

/**
 * Created by preeta kuruvilla on 26/08/2018.
 */
@Entity
public class ParticipantVO {

    @Id
    private Long participantId;
    private String participantName;
    private Long groupId;
    private double liabilityAmount;

    public ParticipantVO() {}

    public ParticipantVO(long participantId, String participantName, long groupId, double liabilityAmount) {
      this.participantId=participantId;
      this.participantName=participantName;
      this.groupId=groupId;
      this.liabilityAmount=liabilityAmount;
    }

    
    public Long getParticipantId() {
		return participantId;
	}

	public void setParticipantId(Long participantId) {
		this.participantId = participantId;
	}

	public String getParticipantName() {
		return participantName;
	}

	public void setParticipantName(String participantName) {
		this.participantName = participantName;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public double getLiabilityAmount() {
		return liabilityAmount;
	}

	public void setLiabilityAmount(double liabilityAmount) {
		this.liabilityAmount = liabilityAmount;
	}

	@Override
    public String toString() {
        return String.format("ParticipantVO[participant_id=[%s], participant_name=[%s], group_id=[%s], liability_amount=[%s]]",
        this.participantId,
        this.participantName,
        this.groupId,
        this.liabilityAmount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParticipantVO participant = (ParticipantVO) o;
        return Objects.equals(participantId, participant.participantId) &&
                Objects.equals(participantName, participant.participantName) &&
                Objects.equals(groupId, participant.groupId) &&
                Objects.equals(liabilityAmount, participant.liabilityAmount);
    }

    @Override
    public int hashCode() {
        int result = 0;
        if (this == null)
            return result;
        result = 31 * result + (this.participantId == null ? 0 : this.participantId.hashCode());
        result = 31 * result + (this.participantName == null ? 0 : this.participantName.hashCode());
        result = 31 * result + (this.groupId == null ? 0 : this.groupId.hashCode());
        return result;
    }
}
