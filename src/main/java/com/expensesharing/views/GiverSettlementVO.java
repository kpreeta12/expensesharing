package com.expensesharing.views;

import javax.persistence.*;
import java.util.Objects;

/**
 * Created by preeta kuruvilla on 26/08/2018.
 */
@Entity
public class GiverSettlementVO {

    @Id
    private Long giverParticipantId;
    private double settlementAmount;

    protected GiverSettlementVO() {}

    public GiverSettlementVO(long giverParticipantId, double settlementAmount) {
      this.giverParticipantId=giverParticipantId;
      this.settlementAmount=settlementAmount;
    }


    public Long getGiverParticipantId() {
		return giverParticipantId;
	}

	public void setGiverParticipantId(Long giverParticipantId) {
		this.giverParticipantId = giverParticipantId;
	}


	public double getSettlementAmount() {
		return settlementAmount;
	}

	public void setSettlementAmount(double settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	@Override
    public String toString() {
        return String.format("GiverSettlementVO[giver_participant_id=[%s],settlement_amount=[%s]",
        this.giverParticipantId);
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GiverSettlementVO participant = (GiverSettlementVO) o;
        return Objects.equals(giverParticipantId, participant.giverParticipantId) &&
                Objects.equals(settlementAmount, participant.settlementAmount);
    }

    @Override
    public int hashCode() {
        int result = 0;
        if (this == null)
            return result;
        result = 31 * result + (this.giverParticipantId == null ? 0 : this.giverParticipantId.hashCode());
        return result;
    }
}
