package com.expensesharing.views;

import javax.persistence.*;
import java.util.Objects;

/**
 * Created by preeta kuruvilla on 26/08/2018.
 */
@Entity
public class ParticipantSpendVO {

    @Id
    private Long participantId;
    private double spendAmount;

    protected ParticipantSpendVO() {}

    public ParticipantSpendVO(long participantId, double spendAmount) {
      this.participantId=participantId;
      this.spendAmount=spendAmount;
    }

    
    public Long getParticipantId() {
		return participantId;
	}

	public void setParticipantId(Long participantId) {
		this.participantId = participantId;
	}

	public double getSpendAmount() {
		return spendAmount;
	}

	public void setSpendAmount(double spendAmount) {
		this.spendAmount = spendAmount;
	}

	@Override
    public String toString() {
        return String.format("ParticipantSpendVO[participant_id=[%s], participant_name=[%s], group_id=[%s], liability_amount=[%s]]",
        this.participantId,
        this.spendAmount);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParticipantSpendVO participant = (ParticipantSpendVO) o;
        return Objects.equals(participantId, participant.participantId) &&
                Objects.equals(spendAmount, participant.spendAmount);
    }

    @Override
    public int hashCode() {
        int result = 0;
        if (this == null)
            return result;
        result = 31 * result + (this.participantId == null ? 0 : this.participantId.hashCode());
        return result;
    }
}
