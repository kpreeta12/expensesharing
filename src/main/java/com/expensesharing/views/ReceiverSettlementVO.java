package com.expensesharing.views;

import javax.persistence.*;
import java.util.Objects;

/**
 * Created by preeta kuruvilla on 26/08/2018.
 */
@Entity
public class ReceiverSettlementVO {

    @Id
    private Long receiverParticipantId;
    private double settlementAmount;

    protected ReceiverSettlementVO() {}

    public ReceiverSettlementVO(long receiverParticipantId, double settlementAmount) {
      this.receiverParticipantId=receiverParticipantId;
      this.settlementAmount=settlementAmount;
    }


	public Long getReceiverParticipantId() {
		return receiverParticipantId;
	}

	public void setReceiverParticipantId(Long receiverParticipantId) {
		this.receiverParticipantId = receiverParticipantId;
	}

	public double getSettlementAmount() {
		return settlementAmount;
	}

	public void setSettlementAmount(double settlementAmount) {
		this.settlementAmount = settlementAmount;
	}

	@Override
    public String toString() {
        return String.format("ReceiverSettlementVO[receiver_participant_id=[%s], settlement_amount=[%s]",
        this.receiverParticipantId);
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReceiverSettlementVO participant = (ReceiverSettlementVO) o;
        return Objects.equals(receiverParticipantId, participant.receiverParticipantId) &&
                Objects.equals(receiverParticipantId, participant.receiverParticipantId) &&
                Objects.equals(settlementAmount, participant.settlementAmount);
    }

    @Override
    public int hashCode() {
        int result = 0;
        if (this == null)
            return result;
        result = 31 * result + (this.receiverParticipantId == null ? 0 : this.receiverParticipantId.hashCode());
        result = 31 * result + (this.receiverParticipantId == null ? 0 : this.receiverParticipantId.hashCode());
        return result;
    }
}
