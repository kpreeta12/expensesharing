package com.expensesharing.utils.mapping;

import com.expensesharing.entities.GroupDO;
import com.expensesharing.views.GroupVO;
import java.util.function.Function;

/**
 * Created by preeta kuruvilla on 26/08/2018..
 */
public class GroupMappingFunction implements Function<GroupDO, GroupVO> {
    @Override
    public GroupVO apply(final GroupDO groupDO) {
        if (groupDO == null) {
            return null;
        }


        final GroupVO groupVO = new GroupVO(groupDO.getGroupId(), groupDO.getGroupName(),
                groupDO.getEventDescription(), groupDO.getNumberOfParticipants(), groupDO.getTotalExpense(),groupDO.getTotalSpent());

        return groupVO;
    }
}
