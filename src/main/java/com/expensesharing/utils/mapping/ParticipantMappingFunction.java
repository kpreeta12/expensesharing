package com.expensesharing.utils.mapping;

import com.expensesharing.entities.ParticipantDO;
import com.expensesharing.views.ParticipantVO;

import java.util.function.Function;

/**
 * Created by preeta kuruvilla on 26/08/2018.
 */
public class ParticipantMappingFunction implements Function<ParticipantDO, ParticipantVO> {
    @Override
    public ParticipantVO apply(final ParticipantDO participantDO) {
        if (participantDO == null)
            return null;


        final ParticipantVO participantVO = new ParticipantVO(participantDO.getParticipantId(),
                participantDO.getParticipantName(),
                participantDO.getGroupId(),
                participantDO.getLiabilityAmount());
        return participantVO;
    }
}
