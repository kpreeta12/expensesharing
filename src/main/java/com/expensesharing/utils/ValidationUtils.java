package com.expensesharing.utils;

import static com.google.common.base.Preconditions.checkNotNull;
import com.expensesharing.exception.BadRequestException;

/**
 * Created by preeta kuruvilla on 26/08/2018.
 */
public class ValidationUtils {

    public static void validateParam(Object paramValue, String errorMessage) {
        try {
            checkNotNull(paramValue);
        } catch (NullPointerException ex) {
            throw new BadRequestException(errorMessage);
        }
    }
}
