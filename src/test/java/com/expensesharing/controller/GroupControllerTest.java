package com.expensesharing.controller;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.expensesharing.services.GroupService;
import com.expensesharing.views.GroupVO;
/**
 * Created by preeta kuruvilla on 26/08/2018.
 */

@RunWith(SpringRunner.class)
@WebMvcTest(value = GroupController.class, secure = false)
public class GroupControllerTest {
	

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Autowired
	private MockMvc mockMvc;
	@MockBean
	private GroupService groupService;
	
	private String sampleJSON = "{\n" + 
			"    \"groupId\": 1001,\n" + 
			"    \"groupName\": \"Group1\",\n" + 
			"    \"eventDescription\": \"Group1 Description\",\n" + 
			"    \"numberOfParticipants\": 3,\n" + 
			"    \"totalExpense\": 100,\n" + 
			"    \"totalSpent\": 0\n" + 
			"}";
	
	private String sampleGroupsJSON = "[{groupId:1001,groupName:Group1,eventDescription:'Group1 Description',numberOfParticipants:3,totalExpense:100,totalSpent:0},{groupId:1002,groupName:Group2,eventDescription:'Group2 Description',numberOfParticipants:3,totalExpense:100,totalSpent:0}]";
			

	
	@Test
	public void testListGroups() throws Exception {
		
        GroupVO mockgroupVO1 = new GroupVO(1001, "Group1", "Group1 Description", 3, 100, 0);
        GroupVO mockgroupVO2 = new GroupVO(1002, "Group2", "Group2 Description", 3, 100, 0);
		List<GroupVO> mockListJSON = new ArrayList<GroupVO>();
		mockListJSON.add(mockgroupVO1);
		mockListJSON.add(mockgroupVO2);		
		Mockito.when(
				groupService.listGroups()).thenReturn(mockListJSON);				
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/Groups").accept(
				MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		System.out.println(result.getResponse());
		JSONAssert.assertEquals(sampleGroupsJSON, result.getResponse()
				.getContentAsString(), false);
	}

	@Test
	public void testGetGroup() throws Exception {				
		GroupVO mockgroupVO = new GroupVO(1001, "Group1", "Group1 Description", 3, 100, 0);
		Mockito.when(
				groupService.getGroup(Mockito.anyLong())).thenReturn(mockgroupVO);				
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/Groups/1001").accept(
				MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		System.out.println(result.getResponse());
		JSONAssert.assertEquals(sampleJSON, result.getResponse()
				.getContentAsString(), false);
	}

	@Test
	public void testCreateGroup() throws Exception {
		
		GroupVO mockgroupVO = new GroupVO(1001, "Group1", "Group1 Description", 3, 100, 0);
		System.out.println("See:"+groupService);
		
		Mockito.when(
				groupService.createGroup(mockgroupVO)).thenReturn(mockgroupVO);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/Groups")
				.accept(MediaType.APPLICATION_JSON).content(sampleJSON)
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		System.out.println("here:"+result.toString());
		MockHttpServletResponse response = result.getResponse();
		System.out.println("response:"+response.toString());
		assertEquals(200, response.getStatus());
	}

}
