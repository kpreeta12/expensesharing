package com.expensesharing.controller;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.expensesharing.services.ParticipantService;
import com.expensesharing.views.GroupVO;
import com.expensesharing.views.ParticipantVO;

@RunWith(SpringRunner.class)
@WebMvcTest(value = ParticipantController.class, secure = false)
public class ParticipantControllerTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private ParticipantService participantService;
	   
	private String sampleJSON = "{\n" + 
			"    \"participantId\": 2001,\n" + 
			"    \"participantName\": \"Participant1\",\n" + 
			"    \"groupId\": 1001,\n" + 
			"    \"liabilityAmount\": 0\n" + 
			"}";
	
	private String sampleParticipantsJSON = "[{participantId:2001,participantName:Participant1,groupId:1001,liabilityAmount:0},{participantId:1002,participantName:Participant2,groupId:1001,liabilityAmount:0},{participantId:2001,participantName:Participant1,groupId:1001,liabilityAmount:0},{participantId:1002,participantName:Participant2,groupId:1001,liabilityAmount:0}]";
			

	
	@Test
	public void testGetParticipant() throws Exception {
		ParticipantVO mockParticipantVO = new ParticipantVO(2001,"Participant1",1001, 0);
		Mockito.when(
				participantService.getParticipant(Mockito.anyLong())).thenReturn(mockParticipantVO);				
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/Participants/2001").accept(
				MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		System.out.println(result.getResponse());
		JSONAssert.assertEquals(sampleJSON, result.getResponse()
				.getContentAsString(), false);
		
	}

	@Test
	public void testListGroups() throws Exception {
		ParticipantVO mockgroupVO1 = new ParticipantVO(2001,"Participant1",1001,0);
        ParticipantVO mockgroupVO2 = new ParticipantVO(2002,"Participant2",1001,0);
		List<ParticipantVO> mockListJSON = new ArrayList<ParticipantVO>();
		mockListJSON.add(mockgroupVO1);
		mockListJSON.add(mockgroupVO2);
		
		Mockito.when(
				participantService.listParticipantsByGroupId(Mockito.anyLong())).thenReturn(mockListJSON);				
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/Participants/all/1001").accept(
				MediaType.APPLICATION_JSON);
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		System.out.println(result.getResponse());
		assertEquals(200, result.getResponse().getStatus());
						
	}

	/*@Test
	public void testDeleteParticipant() {
		fail("Not yet implemented");
	}*/

	@Test
	public void testCreateParticipant() throws Exception {
		ParticipantVO mockparticipantVO = new ParticipantVO(2001,"Participant1",1001,0);
		Mockito.when(participantService.createParticipant(mockparticipantVO)).thenReturn(mockparticipantVO);	
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/Participants")
				.accept(MediaType.APPLICATION_JSON).content(sampleJSON)
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		System.out.println("here:"+result.toString());
		MockHttpServletResponse response = result.getResponse();
		System.out.println("response:"+response.toString());
		assertEquals(200, response.getStatus());
	}

	@Test
	public void testUpdateLiabilityAmtBasedOnSpent() {
		
	}

	@Test
	public void testUpdateLiabilityAmtBasedOnSettlement() {
		
	}

}
