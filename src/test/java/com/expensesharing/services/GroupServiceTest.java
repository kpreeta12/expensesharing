package com.expensesharing.services;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by preeta kuruvilla on 26/08/2018.
 */

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.expensesharing.entities.GroupDO;
import com.expensesharing.repositories.GroupRepository;
import com.expensesharing.repositories.ParticipantRepository;
import com.expensesharing.services.impl.GroupServiceImpl;
import com.expensesharing.views.GroupVO;

@RunWith(SpringRunner.class)
@WebMvcTest(value = ParticipantService.class, secure = false)
//@ContextConfiguration(classes=ParticipantServiceConfig.class)
public class GroupServiceTest {
	
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private GroupService mockgroupService;

	@MockBean
	private GroupRepository mockgroupRepository;

	@MockBean
	private ParticipantRepository mockparticipantRepository;

	private GroupDO mockgroupDO1;
	private GroupDO mockgroupDO2;
	private GroupDO mockgroupDO;
	private GroupVO mockgroupVO;

	@Before
	public void setUp() throws Exception {
		//create mocks for save, findOne and findAll methods
				mockgroupVO = new GroupVO(1001, "Group1", "Group1 Description", 3, 100, 0);
				mockgroupDO = new GroupDO(1001, "Group1", "Group1 Description", 3, 100, 0);
				mockgroupDO1 = new GroupDO(1001, "Group1", "Group1 Description", 3, 100, 0);
		        mockgroupDO2 = new GroupDO(1002, "Group2", "Group2 Description", 3, 100, 0);        
				List<GroupDO> mockListGroup = new ArrayList<GroupDO>();		
				mockListGroup.add(mockgroupDO1);
				mockListGroup.add(mockgroupDO2);
				Mockito.when(
						mockgroupRepository.save(mockgroupDO)).thenReturn(mockgroupDO);
				Mockito.when(
						mockgroupRepository.findOne(Mockito.anyLong())).thenReturn(mockgroupDO);
				Mockito.when(
						mockgroupRepository.findAll()).thenReturn(mockListGroup);
	}

	@After
	public void tearDown() throws Exception {
	}

	
    @Test
	public void testCreateGroup() {
		GroupService participantService=new GroupServiceImpl(mockgroupRepository);
	    GroupVO groupVO=participantService.createGroup(mockgroupVO);
	    assertNotNull(groupVO);
	}
	
	@Test
	public void testGetGroup() {
		GroupService participantService=new GroupServiceImpl(mockgroupRepository);
	    GroupVO groupVO=participantService.getGroup(1001L);
	    assertNotNull(groupVO);
	}
	
	@Test
	public void testListGroups() {
		GroupService participantService=new GroupServiceImpl(mockgroupRepository);
	    List<GroupVO> lstgroups=participantService.listGroups();
	    assertNotNull(lstgroups);
	}

}
