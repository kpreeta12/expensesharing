package com.expensesharing.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import com.expensesharing.entities.GroupDO;
import com.expensesharing.entities.ParticipantDO;
import com.expensesharing.exception.GroupNotFoundException;
import com.expensesharing.repositories.GroupRepository;
import com.expensesharing.repositories.ParticipantRepository;
import com.expensesharing.services.impl.ParticipantServiceImpl;
import com.expensesharing.views.GiverSettlementVO;
import com.expensesharing.views.ParticipantSpendVO;
import com.expensesharing.views.ParticipantVO;
import com.expensesharing.views.ReceiverSettlementVO;

/**
 * Created by preeta kuruvilla on 26/08/2018.
 */

@RunWith(SpringRunner.class)
@WebMvcTest(value = ParticipantService.class, secure = false)
//@ContextConfiguration(classes=ParticipantServiceConfig.class)

public class ParticipantServiceTest {
		
@Autowired
private MockMvc mockMvc;

@MockBean
private GroupService mockgroupService;

@MockBean
private GroupRepository mockgroupRepository;

@MockBean
private ParticipantRepository mockparticipantRepository;




private GroupDO mockgroupDO1;
private GroupDO mockgroupDO2;
private GroupDO mockgroupDO;
private ParticipantVO mockparticipantVO;
private ParticipantDO mockparticipantDO;
private ParticipantVO invalidmockparticipantVO;
private ParticipantDO mockparticipantDO1;
private ParticipantDO mockparticipantDO2;
private ParticipantDO mockparticipantDO3;



	@Before
	public void setUp() throws Exception {
		
		//create mocks for save, findOne and findAll methods
		//mockgroupVO = new GroupVO(1001, "Group1", "Group1 Description", 3, 100, 0);
		mockgroupDO = new GroupDO(1001, "Group1", "Group1 Description", 3, 100, 0);
		mockgroupDO1 = new GroupDO(1001, "Group1", "Group1 Description", 3, 100, 0);
        mockgroupDO2 = new GroupDO(1002, "Group2", "Group2 Description", 3, 100, 0);        
		List<GroupDO> mockListGroup = new ArrayList<GroupDO>();		
		mockListGroup.add(mockgroupDO1);
		mockListGroup.add(mockgroupDO2);
		
		mockparticipantVO = new ParticipantVO(2001, "Participant1",1001,-40);
		mockparticipantDO = new ParticipantDO(2001L, "Participant1",1001L,-40);
		invalidmockparticipantVO = new ParticipantVO(2001, "Participant1",9999,0L);
		mockparticipantDO1 = new ParticipantDO(2001L, "Participant1",1001L,-40);
        mockparticipantDO2 = new ParticipantDO(2002L, "Participant2",1001L,33.3);  
        mockparticipantDO3 = new ParticipantDO(2003L, "Participant2",1001L,33.3); 
		List<ParticipantDO> mockListParticipant = new ArrayList<ParticipantDO>();		
		mockListParticipant.add(mockparticipantDO1);
		mockListParticipant.add(mockparticipantDO2);
		mockListParticipant.add(mockparticipantDO3);
		
		Mockito.when(
				mockgroupRepository.save(mockgroupDO)).thenReturn(mockgroupDO);
		Mockito.when(
				mockgroupRepository.findOne(Mockito.anyLong())).thenReturn(mockgroupDO);
		Mockito.when(
				mockgroupRepository.findAll()).thenReturn(mockListGroup);
		
		
		Mockito.when(
				mockparticipantRepository.save(mockparticipantDO)).thenReturn(mockparticipantDO);
		Mockito.when(
				mockparticipantRepository.findOne(Mockito.anyLong())).thenReturn(mockparticipantDO);
		Mockito.when(
				mockparticipantRepository.findByGroupId(Mockito.anyLong())).thenReturn(mockListParticipant);
		
	}

	@After
	public void tearDown() throws Exception {
	}

/*	@Test
	public void testValidCreateParticipant() throws Exception {	
		
		ParticipantService participantService=new ParticipantServiceImpl(mockparticipantRepository,mockgroupRepository);
	    ParticipantVO participantVO=participantService.createParticipant(mockparticipantVO);
	    assertNotNull(participantVO);
	}*/

	@Test
	public void testInValidCreateParticipant() {
		Mockito.when(
				mockgroupRepository.findOne(Mockito.anyLong())).thenReturn(null);
	    ParticipantService participantService=new ParticipantServiceImpl(mockparticipantRepository,mockgroupRepository);
	    try {
	    ParticipantVO participantVO=participantService.createParticipant(invalidmockparticipantVO);
	    }
	    catch(GroupNotFoundException e) {
	    assert(true);
	    }
	}
	
	
	@Test
	public void testUpdateLiabilityAmtBasedOnSpent() {
		
		ParticipantService participantService=new ParticipantServiceImpl(mockparticipantRepository,mockgroupRepository);		
		ParticipantSpendVO participantSpendVO1=new ParticipantSpendVO(2001,75);
		participantService.updateLiabilityAmtBasedOnSpent(participantSpendVO1);
	    
	    ParticipantSpendVO participantSpendVO2=new ParticipantSpendVO(2002,25);		
	    participantService.updateLiabilityAmtBasedOnSpent(participantSpendVO2);
	    assertEquals(100L, (long)mockgroupDO.getTotalSpent());
	    		
	}
	
	@Test
	public void testUpdateGiverLiabilityAmtBasedOnSettlement() {
		ParticipantService participantService=new ParticipantServiceImpl(mockparticipantRepository,mockgroupRepository);
		GroupDO mockgroupDO=new GroupDO(1001,"Group1", "Group1 Description", 3, 100, 100);
		
		Mockito.when(
				mockgroupRepository.findOne(Mockito.anyLong())).thenReturn(mockgroupDO);
		ParticipantDO mockparticipantDO3=new ParticipantDO(2003L,"Participant3", 1001L, 33.3);
			
		Mockito.when(
				mockparticipantRepository.findOne(Mockito.anyLong())).thenReturn(mockparticipantDO3);	
		double giverliabilitybefore=mockparticipantDO3.getLiabilityAmount();	
		GiverSettlementVO participantSettlementVO=new GiverSettlementVO(mockparticipantDO3.getParticipantId(),10);		
		Mockito.when(
				mockparticipantRepository.save(mockparticipantDO3)).thenReturn(mockparticipantDO3);
		participantService.updateGiverLiabilityAmtBasedOnSettlement(participantSettlementVO);
		double giverliabilityafter=mockparticipantDO3.getLiabilityAmount();		
		assertEquals(10,(long)(giverliabilitybefore-giverliabilityafter));
		
	}
	
	
	
	@Test
	public void testUpdateReceiverLiabilityAmtBasedOnSettlement() {
		ParticipantService participantService=new ParticipantServiceImpl(mockparticipantRepository,mockgroupRepository);
		GroupDO mockgroupDO=new GroupDO(1001,"Group1", "Group1 Description", 3, 100, 100);
		
		Mockito.when(
				mockgroupRepository.findOne(Mockito.anyLong())).thenReturn(mockgroupDO);
		ParticipantDO mockparticipantDO1=new ParticipantDO(2001L,"Participant1", 1001L, 33.3);
		
		Mockito.when(
				mockparticipantRepository.findOne(Mockito.anyLong())).thenReturn(mockparticipantDO1);	
		double giverliabilitybefore=mockparticipantDO1.getLiabilityAmount();	
		ReceiverSettlementVO participantSettlementVO=new ReceiverSettlementVO(mockparticipantDO1.getParticipantId(),10);		
		Mockito.when(
				mockparticipantRepository.save(mockparticipantDO1)).thenReturn(mockparticipantDO3);
		participantService.updateReceiverLiabilityAmtBasedOnSettlement(participantSettlementVO);
		double giverliabilityafter=mockparticipantDO1.getLiabilityAmount();		
		assertEquals(10,(long)(giverliabilityafter-giverliabilitybefore));
		
	}

	@Test
	public void testGetParticipant() {
		ParticipantService participantService=new ParticipantServiceImpl(mockparticipantRepository,mockgroupRepository);		
		ParticipantVO participantVO=participantService.getParticipant(2001L);
	    assertNotNull(participantVO);
	}
/*
	@Test
	public void testDeleteParticipant() {
		fail("Not yet implemented");
	}
*/
	@Test
	public void testListParticipantsByGroupId() {
		ParticipantService participantService=new ParticipantServiceImpl(mockparticipantRepository,mockgroupRepository);		
		List<ParticipantVO> lstparticipant=participantService.listParticipantsByGroupId(1001L);
	    assertNotNull(lstparticipant);
	}

}
